from src.conditionals_excersises import



def test_check_person_under_adult_age():
    test_is_adult = is_person_an_adult(17)
    assert test_is_adult == False

def test_check_person_is_adult_age():
    test_is_adult = is_person_an_adult(18)
    assert test_is_adult == True

def test_check_person_is_also_adult_age():
    test_is_adult = is_person_an_adult(19)
    assert test_is_adult == True