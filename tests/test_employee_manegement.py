from src.employee_manegement import generate_random_number



def test_single_employee_generation_seniority_years_have_correct_value():
    test_seniority_years = generate_random_number()
    assert test_seniority_years >= 0
    assert test_seniority_years <= 40
    assert isinstance(test_seniority_years, int)

#def test_single_employee_generated_has_propoer_keys():
    #test_employee = get_dictionary_with_random_personal_data()
    #assert 'email' in test_employee.keys()
    #assert 'seniority_years' in test_employee.keys()
    #assert 'female' in test_employee.keys()