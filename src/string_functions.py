def print_greetings_for_a_person_in_the_city(name, city):
    print(f"Witaj {name}! Miło Cię widzieć w naszym mieście: {city}!")


def generator_of_emails_in_four_testers_pl(imie, nazwisko):
    return (f"{imie.lower()}.{nazwisko.lower()}@4testers.pl")


if __name__ == '__main__':
    print_greetings_for_a_person_in_the_city("Kasia", "Szczecin")
    print(generator_of_emails_in_four_testers_pl("Janusz", "Nowak"))
    print(generator_of_emails_in_four_testers_pl("Barbara", "Kowalska"))


