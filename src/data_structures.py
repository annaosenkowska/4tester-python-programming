movies = ['Dune', 'Stare Wars', 'Blade Runner', 'Stalker', 'Lost Highway']

last_movie = movies[-1]
movies.append('LOTR')
movies.append('Titanic')
print(len(movies))

middle_movies = movies[2:5]
print(middle_movies)

movies.insert(0, 'Top Gun 2')
print(movies)

emails = ['a@example.com', 'b@example.com']

print(emails)
print(len(emails))
last_emails = emails[-1]
print(last_emails)
print(emails[0])
emails.append('c@examples.com')
print(len(emails))
print(emails)

friend = {

    'name': 'Marta',
    'age': 31,
    'hobby': ['sailing', 'climbing']
}

print(friend)

friend_hobbies = friend['hobby']
print("Hobbies of my friend:", friend_hobbies)
print(f'My friend has {len(friend_hobbies)} hobbies')
friend['hobby'].append('football')
print(friend)
friend['married'] = True
