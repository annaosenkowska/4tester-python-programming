# funkcje

def number_squared(number):
    return number ** 2


print(number_squared(0))
print(number_squared(16))
print(number_squared(2.55))


def concerter_Celsius_degree_to_Farenheit(celcius_degree):
    return (celcius_degree * 1.8) + 32


temperature = concerter_Celsius_degree_to_Farenheit(20)
print(temperature)
print(f"20 stopni Celsjusza to : {temperature} stopni Farenheita")
print(concerter_Celsius_degree_to_Farenheit(20))
print("20 stopni Celsjusza to : ", concerter_Celsius_degree_to_Farenheit(20), " stopni Farenheita")


def volume_of_a_cuboid(a, b, c):
    return a * b * c


print(volume_of_a_cuboid(3, 5, 7))


def welcome_in_city(name, city):
    print(f"Miło Cię {name} widzieć w mieście {city}!")


welcome_in_city("Ania", "Poznań")


def generator_of_emails_in_4testerspl(name, surname):
    return f"{name.lower()}.{surname.lower()}@4testers.pl"


print(generator_of_emails_in_4testerspl('Janusz', 'Nowak'))
print(generator_of_emails_in_4testerspl('Anna', 'Osenkowska'))

# listy

names = ['Ania', 'Pawel', 'Marek', 'Kasia', 'Gosia']

last_name = names[-1]
print(last_name)
names.append('Basia')
print(names)
print(len(names))
middle_names = names[1:3]
print(middle_names)
names.insert(1, 'Hubert')
print(names)

emails = ['a.osa@gmail.com', 'basia.osa@gmail.com']
print(len(emails))
first_email = emails[0]
print(first_email)
last_email = emails[-1]
print(last_email)
emails.append('c@jdjd.com')
print(emails)

# słowniki

friend = {
    "name": "Marta",
    "age": 31,
    "hobby": ["sailing", "climbing"]
}
friend_hobbies = friend["hobby"]
print(friend_hobbies)
print(f"My friend has {len(friend_hobbies)}")
friend["hobby"].append("football")
print(friend)
friend["married"] = True
print(friend)


def calculate_sum_of_numbers_in_the_list(list):
    return sum(list)


def calculate_average_of_two_numbers(a, b):
    return a + b / 2


january = [-4, 1.0, -7, 2]
february = [-13, -9, -3, 3]

print(calculate_sum_of_numbers_in_the_list(january))
print(calculate_sum_of_numbers_in_the_list(february))

import random
import string


def generate_random_password():
    characters = string.ascii_letters + string.digits + string.punctuation
    return ''.join(random.choice(string.printable) for i in range(8))


def data_to_login(user):
    random_password = generate_random_password()
    return {
        "email": f"{user}@exaples.com",
        "password": random_password
    }


print(data_to_login("ania"))
print(data_to_login("basia"))
print(data_to_login("staszek"))


def describe_of_player(player_dictionary):
    nick = player_dictionary['nick']
    type = player_dictionary['type']
    exp_points = player_dictionary['exp_points']
    print(f"The player {nick} is of type {type} and has {exp_points} EXP")

player_1 = {
        "nick": "maestro_54",
        "type": "warrior",
        "exp_points": 3000
    }

describe_of_player(player_1)


fruit = "oranges"
print(fruit.capitalize())

