class Product:
    def __init__(self, name: str, unit_price: float, quantity: int = 1):
        self.name = name
        self.unit_price = unit_price
        self.quantity = quantity

    def get_price(self):
        return self.unit_price * self.quantity


class Order:
    def __init__(self, customer_email: str):
        self.customer_email = customer_email
        self.products = []
        self.purchased = False

    def add_product(self, product: Product):
        self.products.append(product)
        # self.purchase()

    def get_total_price(self):
        total_price = 0
        for product in self.products:
            total_price += product.get_price()
        return total_price

    def get_total_quantity_of_products(self):
        total_quantity_of_products = 0
        for product in self.products:
            total_quantity_of_products += product.quantity
        return total_quantity_of_products

    def purchase(self):
        if self.get_total_quantity_of_products() != 0:
            self.purchased = True

if __name__ == '__main__':
    order = Order("a.osa@op.pl")
    order.add_product(Product("czapka", 30, 2))
    order.add_product(Product('szynka', 30, 2))

    print('lista wszystkich obiektow w zamowieniu', order.products)
    for product in order.products:
        print('produkty w zamowieniu', product.name, product.unit_price, product.quantity)

    print('calkowita liczba produktow', order.get_total_quantity_of_products())






