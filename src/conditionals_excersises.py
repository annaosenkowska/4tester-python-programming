def display_speed_information(speed):
    if speed == 50:
        print("Tkank you, your speed is below limit! :)")
    else:
        print("Slow down! :(")


def weather_conditions(temperature_in_Celcius, preassure_in_hektopascal):
    if temperature_in_Celcius == 0 and preassure_in_hektopascal == 1013:
        print(True)
    else:
        print(False)


def is_person_an_adult(age):
    if age >= 18:
        return True
    else:
        return False


def calculate_fine_amount(speed):
    return 500 + (speed - 50) * 10


def print_the_value_of_speeding_fine_in_built_up_area(speed):
    if speed > 100:
        print("You just lost your driving license")
    elif speed > 50:
        print(f"You just got a fine! Fine amount {calculate_fine_amount(speed)}")
    else:
        print("Thank You, your speed is fine")


def evaluations(note):
    if note > 5 or note < 2:
        return "N/Y"
    elif note >= 4.5 and note <= 5:
        return "bardzo dobry"
    elif note >= 4:
        return "dobry"
    elif note >= 3:
        return "dostateczny"
    else:
        return "niedostateczny"

        #    if not (isinstance(note, float)) or (isinstance(note, int)):
        return "N/Y"


if __name__ == '__main__':
    display_speed_information(50)
    display_speed_information(49)
    display_speed_information(51)

    weather_conditions(0, 1013)
    weather_conditions(1, 1013)
    weather_conditions(0, 1014)
    weather_conditions(1, 1014)

    print_the_value_of_speeding_fine_in_built_up_area(101)
    print_the_value_of_speeding_fine_in_built_up_area(100)
    print_the_value_of_speeding_fine_in_built_up_area(49)
    print_the_value_of_speeding_fine_in_built_up_area(50)

    print(evaluations(5.1))
