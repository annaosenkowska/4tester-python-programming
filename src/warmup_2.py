def get_number_squared(number):
    return (number ** 2)


def converter_celcius_degree_to_farenheit(number):
    return (number * 9 / 5 + 32)


def volume_of_a_cuboid(a, b, c):
    return (a * b * c)


if __name__ == '__main__':
    # Zadanie 1
    print(get_number_squared(0))
    print(get_number_squared(16))
    print(get_number_squared(2.55))

    # Zadanie 2
    print(converter_celcius_degree_to_farenheit(20))

    # Zadanie 3
    print(volume_of_a_cuboid(3, 5, 7))
