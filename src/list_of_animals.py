animals = [
    {"name": "Burek", "kind": "dog", "age": 7},
    {"name": "Bari", "kind": "cat", "age": None},
    {"name": "Misio", "kind": "hamster", "age": 1},

]
print(animals[-1]["name"])
animals[1]["age"] = 2
print(animals[1])

addresses = [
    {"city": "Katowice", "street": "Bielska", "house_number": 4, "post_code": "40-749"},
    {"city": "Kraków", "street": "Długa", "house_number": 10, "post_code": "30-323"},
    {"city": "Wrocław", "street": "Wodna", "house_number": 14, "post_code": "20-897"}
]

if __name__ == '__main__':
    print(addresses[-1]["post_code"])
    print(addresses[1]["city"])
    addresses[0]["street"] = "Morska"
    print(addresses)
