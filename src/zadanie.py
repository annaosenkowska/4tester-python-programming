import random

# Listy zawierające dane do losowania
female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Ania`', 'Eryka']
male_fnames = ['Damian', 'Bob', 'Jan', 'Hans', 'Mateusz', 'Marek']
surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']


# Losowanie imienia żeńskiego z listy
def generate_female_firstname():
    return random.choice(female_fnames)


# Losowanie imienia męskiego z listy
def generate_male_firstname():
    return random.choice(male_fnames)


# Losowanie nazwiska z listy
def generate_lastname():
    return random.choice(surnames)


# Losowanie kraju z listy
def generate_random_country():
    return random.choice(countries)


# Losowanie wieku
def generate_random_age():
    return random.randint(5, 45)


# Czy dorosły
def checking_if_the_person_is_an_adult(age):
    if age >= 18:
        return "True"
    else:
        return "False"


# Przykładowy wygenerowany pojedynczy słownik
example_dictionary = {
    'firstname': 'Kate',
    'lastname': 'Yu',
    'email': 'kate.yu@example.com',
    'age': 23,
    'country': 'Poland',
    'adult': True
}


def get_female_dictionary_with_random_personal_data():
    random_female_firstname = generate_female_firstname()
    random_surname = generate_lastname()
    random_country = generate_random_country()
    random_female_email = f'{random_female_firstname.lower()}.{random_surname.lower()}@example.com'
    random_age = generate_random_age()
    adult = checking_if_the_person_is_an_adult(random_age)
    birth_year = 2022 - generate_random_age()
    return {
        "firstname": random_female_firstname,
        "lastname": random_surname,
        "country": random_country,
        "email": random_female_email,
        "age": random_age,
        "adult": adult,
        "birth_year": birth_year

    }


def get_male_dictionary_with_random_personal_data():
    random_male_firstname = generate_male_firstname()
    random_surname = generate_lastname()
    random_country = generate_random_country()
    random_male_email = f'{random_male_firstname.lower()}.{random_surname.lower()}@example.com'
    random_age = generate_random_age()
    adult = checking_if_the_person_is_an_adult(random_age)
    birth_year = 2022 - generate_random_age()
    return {
        "firstname": random_male_firstname,
        "lastname": random_surname,
        "country": random_country,
        "email": random_male_email,
        "age": random_age,
        "adult": adult,
        "birth_year": birth_year

    }


def get_list_of_dictionaries_with_random_personal_data(number_of_elements):
    list_of_dictionary = []
    for element in range(number_of_elements):
        list_of_dictionary.append(get_female_dictionary_with_random_personal_data())
        list_of_dictionary.append(get_male_dictionary_with_random_personal_data())
    return list_of_dictionary


if __name__ == '__main__':
    list_of_dictionary = get_list_of_dictionaries_with_random_personal_data(5)
    print(list_of_dictionary)
    for element in list_of_dictionary:
        print(f'Hi! I am {element["firstname"]}. I come from {element["country"]} and I was born in {element["birth_year"]}')
