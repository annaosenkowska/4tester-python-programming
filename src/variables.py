first_name = "Ania"
last_name = "Osenkowska"
age = 30

print(first_name)
print(last_name)
print(age)

# I am defining a set of variables describing my dog

name = "Pamela"
age_in_months = 5
weight = 5.1
number_of_paws = 4

print(name, age_in_months, weight, number_of_paws)

# I am defining a set of variables describing my friend

name = "Marta"
age = 31
number_of_animals = 0
driving_licence = "True"
number_of_year_our_friendship = 16.5

print(name, age, number_of_animals, driving_licence, number_of_year_our_friendship, sep=',')
