from random import randint
from math import sqrt


def print_hello_40_times():
    for i in range(1, 41):
        print("hello", i)


def print_positive_numbers_from_1_to_30():
    for i in range(1, 31):
        print(i)


def print_positive_numbers(start, stop):
    for i in range(start, stop + 1):
        if i % 7 == 0:
            print(i)


def print_n_random_numbers(n):
    for i in range(n):
        print(randint(1, 1000))


def print_sqare_roots_of_numbers(list_of_numbers):
    for number in list_of_numbers:
        print(sqrt(number))


def get_sqare_roots_of_numbers(list_of_numbers):
    square_roots = []
    for number in list_of_numbers:
        square_roots.append(sqrt(number))
    return square_roots


def get_list_of_temperature_in_farenheit(temps_celsius):
    temps_farenheit = []
    for temp in temps_celsius:
        temps_farenheit.append(temp * 9 / 5 + 32)
    return temps_farenheit


def get_unical_letters_from_input_string(string):
    unique_chars = []
    for i in string:
        if i not in unique_chars:
            unique_chars.append(i)
    return "".join(unique_chars)


def unique_characters(string):
    return ''.join(set(string))

input_string = "bbbacdb"
unique_chars = unique_characters(input_string)
print(unique_chars)


if __name__ == '__main__':
    print_hello_40_times()
    print_positive_numbers_from_1_to_30()
    print_positive_numbers(1, 30)
    print_n_random_numbers(10)
    print(sqrt(16))
    list_of_measurement_result = [11, 12, 3, 2, 4]
    print_sqare_roots_of_numbers(list_of_measurement_result)

    square_roots_of_measurement = get_sqare_roots_of_numbers(list_of_measurement_result)
    print(square_roots_of_measurement)

    temps_celsius = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]
    print(get_list_of_temperature_in_farenheit(temps_celsius))

    input_string = "bbbacdb"
    print(get_unical_letters_from_input_string(input_string))

    print(unique_characters(input_string))

