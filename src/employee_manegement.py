import random
import time


def generate_random_email():
    domain = "examples.com"
    name_list = ["james", "kate", "ania", "basia"]
    random_name = random.choice(name_list)
    # suffix = random.randint(1, 1_000_000)
    suffix = time.time()
    return f'{random_name}.{suffix}@{domain}'


def generate_random_number():
    return random.randint(0, 40)


def generate_random_boolean():
    return random.choice([True, False])


def get_dictionary_with_random_personal_data():
    random_email = generate_random_email()
    random_number = generate_random_number()
    random_boolean = generate_random_boolean()
    return {
        "email": random_email,
        "seniority_years": random_number,
        "female": random_boolean
    }


def get_list_of_dictionaries_with_random_personal_data(number_of_dictionaries):
    list_of_dictionaries = []
    for number in range(number_of_dictionaries):
        list_of_dictionaries.append(get_dictionary_with_random_personal_data())
    return list_of_dictionaries


def return_emails_of_employees_from_dictionaries_if_they_working_more_than_ten_years(personal_data):
    list_of_email = []
    for dictionary in personal_data:
        if dictionary["seniority_years"] > 10:
            list_of_email.append(dictionary["email"])
    return list_of_email


def return_female_dictionaries_of_employee(personal_data):
    list_of_female_dictionaries = []
    for dictionary in personal_data:
        if dictionary["female"] == True:
            list_of_female_dictionaries.append(dictionary)
    return list_of_female_dictionaries


if __name__ == '__main__':
    print(get_dictionary_with_random_personal_data())
    personal_data = get_list_of_dictionaries_with_random_personal_data(10)
    print(personal_data)
    print(return_emails_of_employees_from_dictionaries_if_they_working_more_than_ten_years(personal_data))
    print(return_female_dictionaries_of_employee(personal_data))
