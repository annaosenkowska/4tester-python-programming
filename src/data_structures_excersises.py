def calculate_average_of_list_numbers(input_list):
    return sum(input_list) / len(input_list)


def calculate_average_of_two_numbers(a, b):
    return ((a + b) / 2)


if __name__ == '__main__':
    january = [-4, 1.0, -7, 2]
    february = [-13, -9, -3, 3]
    january_average = calculate_average_of_list_numbers(january)
    february_average = calculate_average_of_list_numbers(february)
    bimonthly_average = calculate_average_of_two_numbers(january_average, february_average)
    print(bimonthly_average)


import secrets
import string

# secure random string
secure_str = ''.join((secrets.choice(string.ascii_letters) for i in range(8)))
print(secure_str)

def data_to_login(email):
    random_password = secure_str()
return ('email': email, 'password': random_password)

print()